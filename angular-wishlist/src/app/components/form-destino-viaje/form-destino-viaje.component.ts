import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, filter, debounce, distinctUntilChanged, switchMap, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud=5;
  searchResults :String[];

  constructor(fb: FormBuilder) {
      this.onItemAdded = new EventEmitter();
      this.fg = fb.group({
       // nombre: ['', Validators.required],
        nombre: ['', Validators.compose([
          Validators.required,
          this.nombreValidator,
          this.nombreValidadorParametrizable(this.minLongitud)
        ])],
        url:['', Validators.required]
      });
   }

  ngOnInit(): void {

    let elementNombre = <HTMLInputElement> document.getElementById('nombre');
    fromEvent(elementNombre, 'input')
    .pipe(
      map((e:KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text=>text.length>=4),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(()=>ajax('/assets/datos.json'))
      
    ).subscribe(ajaxResponse => {
      console.log(ajaxResponse)
      this.searchResults = ajaxResponse.response;
    })
  }

  guardar(nombre: String, url: String) : boolean {

    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): {[s:string]: boolean} {
    let l= control.value.toString().trim().length;
    if(l>0 && l<5 ) {
      return { invalidNombre: true}
    }
    return null;
  }
  
  nombreValidadorParametrizable ( minLog: number) : ValidatorFn { 
    return (control : FormControl) : {[s:string]: boolean} => {
      let l= control.value.toString().trim().length;
      if(l>0 && l< minLog ) {
        return { minLongNombre: true}
      }
      return null;
    }
  }

}
