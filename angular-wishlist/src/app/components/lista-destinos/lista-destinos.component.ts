import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinoApiClient } from '../models/destino-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction, EliminarDestinoAction } from '../models/destino-viajes-state.model';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})

export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates :String[];
  //current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
  listaDestinos; //: DestinoViaje[];
  favorito: DestinoViaje;

  constructor(public destinoApiclient: DestinoApiClient, public store:Store<AppState>) { 
      this.onItemAdded = new EventEmitter();
      this.destinos= [];
      this.updates = [];


      this.store.select(state=>{
          //this.listaDestinos = state.destinos.items;
          return state.destinos.favorito;
        })
        .subscribe(data=> {
          if(data != null) {
            this.updates.push('Se ha eligido a '+ data.nombre);
            this.favorito= data;
          }
      });

      this.store.select(state => state.destinos.items).subscribe(items=>this.listaDestinos = items);

      /*
          this.destinoApiclient.subscribedOnChange((d:DestinoViaje) => {
            console.log(d);
            if(d != null) {
              this.updates.push('Se ha eligido a '+ d.nombre);
            }
            console.log("PAZZZZZZZZ")
          })
          
      */
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) {
    //this.store.dispatch(new NuevoDestinoAction(d));
    this.destinoApiclient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d:DestinoViaje, index:number){

    this.destinoApiclient.elegir(d);
    //return this.store.dispatch(new ElegidoFavoritoAction(d));
   //this.destinoApiclient.elegir(d);
   /* this.destinoApiclient.getAll().forEach(x=> x.setSelected(false));
    d.setSelected(true);
    */
  }

  delete(d:DestinoViaje) {
    this.destinoApiclient.eliminar(d);
    //return this.store.dispatch(new EliminarDestinoAction(d));
  }
  getAll() {
    return this.listaDestinos;
  }


  agregar(titulo: HTMLInputElement)	{
    console.log(titulo.value)
  }

  guardar(nombre:String, url:String):boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    console.log(this.destinos);
    return false;
  }

  limpiar(nombre: HTMLInputElement, url:HTMLInputElement)	{
    nombre.value="";
    url.value="";
  }


/*
  elegido(d:DestinoViaje){
    this.destinos.forEach(function(x){ x.setSelected(false)});
    d.setSelected(true);

  }*/

}
