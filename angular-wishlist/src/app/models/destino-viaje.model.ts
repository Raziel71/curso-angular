import {v4 as uuidv4 } from 'uuid';

export class DestinoViaje{
    public id:number;
    public selected:Boolean;
    servicios: String[];
    destinoId=uuidv4 ();
    constructor(public nombre:String, public imagenUrl: String, public votes:number = 0) {
        
        this.servicios = ['pileta', 'desayuno'];
    }
    
    isSelected (): Boolean{
        return this.selected;
    }
    setSelected(value: Boolean){
        console.log("Value : " + value);
        this.selected = value;
    }
    voteUp() {
        this.votes++;
    }
    voteDown() {
        this.votes--;
    }
    resetVotos(){
        this.votes=0;
    }
}
