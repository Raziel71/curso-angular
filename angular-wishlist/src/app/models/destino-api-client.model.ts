import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction, EliminarDestinoAction, ResetVotosAction } from './destino-viajes-state.model';

@Injectable({
  providedIn: 'root'
})
export class DestinoApiClient{

    constructor(private store: Store<AppState>){ 
     }

     add(d: DestinoViaje){
         this.store.dispatch(new NuevoDestinoAction(d));
     }
     
     elegir(d: DestinoViaje){
         this.store.dispatch(new ElegidoFavoritoAction(d));
     }
     
     eliminar(d: DestinoViaje){
         this.store.dispatch(new EliminarDestinoAction(d));
     }

     reset(d:DestinoViaje) {
         this.store.dispatch(new ResetVotosAction(d));
     }
     

}