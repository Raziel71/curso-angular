export class DestinoViaje {
  private selected: boolean;
  public servicios: string[];
  constructor(public nombre: string, public imagenUrl: string) {
    this.servicios = ['Pileta', 'Desayuno', 'Gimnasio'];
  }
  isSelected(): boolean {
    return this.selected;
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
}
