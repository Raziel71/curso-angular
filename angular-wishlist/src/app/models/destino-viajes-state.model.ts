import {Injectable} from '@angular/core'
import {Action} from '@ngrx/store'
import {Actions, Effect, ofType} from '@ngrx/effects'
import {Observable, of } from 'rxjs'
import {map} from 'rxjs/operators'
import { DestinoViaje } from './destino-viaje.model'

// ESTADO
export interface DestinosViajesState{
    items: DestinoViaje[];
    loading: boolean;
    favorito:DestinoViaje;

}


export const initializeDestinosViajesState = function() {
    return {
        items:[],
        loading: false, 
        favorito: null
    }
}

// ACCIONES
export enum DestinoViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    ELIMINAR_DESTINO = '[Destinos Viajes] Eliminar',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    RESET_VOTOS =  '[Destinos Viajes] Reset Votos'
}

export class NuevoDestinoAction implements Action {
    type = DestinoViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinoViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {}
}

export class EliminarDestinoAction implements Action {
    type = DestinoViajesActionTypes.ELIMINAR_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class VoteUPAction implements Action {
    type = DestinoViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) {}
}
export class VoteDownAction implements Action {
    type = DestinoViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) {}
}

export class ResetVotosAction implements Action {
    type = DestinoViajesActionTypes.RESET_VOTOS;
    constructor(public destino: DestinoViaje) {}
}

export type DestinosViajesAction = NuevoDestinoAction | ElegidoFavoritoAction | EliminarDestinoAction | VoteDownAction | VoteUPAction;

// Reducers:

export function reduceDestinosViajes( state: DestinosViajesState, action: DestinosViajesAction) : DestinosViajesState {
    switch(action.type){
        case DestinoViajesActionTypes.NUEVO_DESTINO: {
             /* return Object.assign({}, state, {
                items: [
                  ...state.items,
                  (action as NuevoDestinoAction).destino
                ]
              })
                */
            return {
                 ...state,
                 items: [...state.items, (action as NuevoDestinoAction).destino]
            };
            
        }

        case DestinoViajesActionTypes.ELEGIDO_FAVORITO : {
            console.log("REDUX: FAVORITO")
            //if(state.items.length>1){
            /*
            const myItems = state.items.map(x => ({...x, selected:false}) );
           }else{

            }*/
           let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
             let myfavorito = new DestinoViaje(fav.nombre, fav.imagenUrl);
             myfavorito.setSelected(true);
            fav = Object.assign({},fav, myfavorito);


            return  Object.assign({}, state, {
                items:  state.items.map((item, index) => {
                      let value= false;
                      if(fav.id == item.id){
                          value= true;
                      }
                      return Object.assign({}, item, {
                        selected: value
                      })
                  }),
                favorito: Object.assign({}, fav, {
                    selected: true
                  })
              })
        }
        case DestinoViajesActionTypes.ELIMINAR_DESTINO : {
            let destinoEliminar: DestinoViaje = (action as EliminarDestinoAction).destino;

            return  Object.assign({}, state, {
                // items:  state.items.filter((d)=>{ d.id!=destinoEliminar.id}),
                items: state.items.filter(({ id }) => id !== destinoEliminar.id)
              })

        }

        case DestinoViajesActionTypes.VOTE_UP : {
            let d: DestinoViaje = (action as VoteUPAction).destino;
            let dest = new DestinoViaje(d.nombre, d.imagenUrl, d.votes);
            dest.voteUp()

            return Object.assign({}, state, {
                    items: state.items.map((item, index) => {
                                return (d.id == item.id)  ? Object.assign({}, item, dest ) : item; 
                        })
                    })

        }

        case DestinoViajesActionTypes.VOTE_DOWN : {
            let d: DestinoViaje = (action as VoteDownAction).destino;
            let dest = new DestinoViaje(d.nombre, d.imagenUrl, d.votes);
            dest.voteDown()
           
            return Object.assign({}, state, {
                    items: state.items.map((item, index) => {
                                return (d.id == item.id)  ? Object.assign({}, item, dest ) : item; 
                        })
                    })

        }

        case DestinoViajesActionTypes.RESET_VOTOS : {
            let d: DestinoViaje = (action as VoteDownAction).destino;
            let dest = new DestinoViaje(d.nombre, d.imagenUrl, d.votes);
            dest.resetVotos()
           
            return Object.assign({}, state, {
                    items: state.items.map((item, index) => {
                                return (d.id == item.id)  ? Object.assign({}, item, dest ) : item; 
                        })
                    })

        }
        default: return state;
    }
}



//effects
@Injectable({
    providedIn: 'root'
  })
export class DestinosViajesEffects {

    @Effect()
    nuevoAgregado$: Observable<Action> =  this.actions$.pipe(
        ofType(DestinoViajesActionTypes.NUEVO_DESTINO),
        map((action:NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    constructor(public actions$: Actions) {}
}
