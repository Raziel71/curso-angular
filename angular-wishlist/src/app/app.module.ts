import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { GuiaUsoComponent } from './guia-uso/guia-uso.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component'
import { DestinosViajesState, reduceDestinosViajes, initializeDestinosViajesState, DestinosViajesEffects } from './models/destino-viajes-state.model';
import { ActionReducerMap, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';



const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch:'full'},
  {path: 'home', component:ListaDestinosComponent},
  {path: 'destino', component:DestinoDetalleComponent},
  {path: 'destino/:destinoId', component:DestinoDetalleComponent},

];

// Redux: init
// estado global
export interface AppState{
  destinos: DestinosViajesState;
}
// join with the reducer, reducars gglobales de la apllicacion.
const reducers: ActionReducerMap<AppState> = {
  destinos: reduceDestinosViajes
}

const reducersInitialState = {
  destinos: initializeDestinosViajesState()
}
// Redux: fin init


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    GuiaUsoComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, 
      {initialState: reducersInitialState,  
        runtimeChecks: {
          strictStateImmutability: false,
          strictActionImmutability: false,
        }
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument({
      //maxAge: 25, // Retains last 25 states
     // logOnly: environment.production, // Restrict extension to log-only mode
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
